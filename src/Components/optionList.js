import React, { Component } from 'react';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import FormHelperText from '@material-ui/core/FormHelperText';

export default class OptionList extends Component{

  constructor(props){
    super(props);
    // pour envoyer la valeur de l'option lors du changments
    this.handleChange = this.handleChange.bind(this);
}

 // pour envoyer la valeur de l'option lors du changments
  handleChange(e){
    var newval =  e.target.value;
    this.props.onChange(newval);
  }

 render(){
     return(
     <FormControl style = {{width : 100, marginLeft : 50, marginTop : 40 , opacity : 0.90}}>
        <NativeSelect
         name="age"
         inputProps={{ 'aria-label': 'age' }}
         onChange = {this.handleChange}
        >
          <option key="-1" value="-1">{this.props.title}</option>
          {this.props.options.map(item => <option key={item.key} value={item.value + '/' + item.label}>{item.label}</option>)}
        </NativeSelect>
        <FormHelperText>Countries</FormHelperText>
      </FormControl>
     )
 }
}   