import React, { Component } from 'react';
import OptionList from './../Components/optionList.js'; 


export default class MonthsList extends Component{
    constructor(props){
        super(props);
  
       this.state = {
          data: []
        }

         // pour envoyer la valeur de l'option lors du changments
        this.handleOptionChange = this.handleOptionChange.bind(this);
      }

      componentDidMount(){

        var items = [
            {
                key: 1, 
                value: 1, 
                label: 'January'
            }, 
            {
                key: 2, 
                value: 2, 
                label: 'February'
            }, 
            {
                key: 3, 
                value: 3, 
                label: 'March'
            }, 
            {
                key: 4, 
                value: 4, 
                label: 'April'
            }, 
            {
                key: 5, 
                value: 5, 
                label: 'May'
            },
            {
                key: 6, 
                value: 6, 
                label: 'June'
            },
            {
                key: 7, 
                value: 7, 
                label: 'July'
            },
            {
                key: 8, 
                value: 8, 
                label: 'August'
            },
            {
                key: 9, 
                value: 9, 
                label: 'September'
            },
            {
                key: 10, 
                value: 10, 
                label: 'October'
            },
            {
                key: 11, 
                value: 11, 
                label: 'November'
            },
            {
                key: 12, 
                value: 12, 
                label: 'December'
            }
        ]
    
          this.setState({
              data: items
          })
      }

       // pour envoyer la valeur de l'option lors du changments
      handleOptionChange(childValue) {
        this.props.onChange(childValue);
      }


    render(){
        return(
            <OptionList title='Choose' options = {this.state.data} onChange={this.handleOptionChange}/>
        )
    }
}