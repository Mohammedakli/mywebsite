import React, { Component } from 'react';
import OptionList from './../Components/optionList.js'; 

function compareStrings(a, b) {
    // Assuming you want case-insensitive comparison
    a = a.toLowerCase();
    b = b.toLowerCase();
  
    return (a < b) ? -1 : (a > b) ? 1 : 0;
  }

export default class CountriesList extends Component{
    constructor(props){
        super(props);

         // pour envoyer la valeur de l'option lors du changments
         this.handleOptionChange = this.handleOptionChange.bind(this);

       this.state = {
          data: []
        }
      }

      componentDidMount(){
        fetch('https://api.covid19api.com/countries')
        .then(res => res.json())
        .then((data) => {
            data.sort(function(a, b) {
                return compareStrings(a.Country, b.Country);
              })
          var items = []; 
          for(var i = 0; i < data.length; i++){
              items.push({key : i, value: data[i].ISO2 , label: data[i].Country }); 
          }
    
          this.setState({
              data: items
          })
      })
      .catch(console.log)
      }

        // pour envoyer la valeur de l'option lors du changments
        handleOptionChange(childValue) {
            this.props.onChange(childValue);
            }
            
    render(){
        return(
            <OptionList title='Choose' options = {this.state.data}  onChange={this.handleOptionChange}/>
        )
    }
}