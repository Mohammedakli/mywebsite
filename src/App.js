import React, { Component } from 'react';
import Home from './pages/Home'
import {BrowserRouter as Router, Route, Switch, Link, Redirect }  from "react-router-dom" 
import AboutUs from './pages/AboutUs' 
import Contamination from './pages/Contamination'

export default class App extends Component{
  render(){
    return(
      <Router>
        <Switch>
        <Route exact path = "/" component = {Home}></Route>
        <Route exact path = "/AboutUs" component = {AboutUs}></Route>
        <Route exact path =  "/avoidContamination" component = {Contamination}></Route>
        </Switch>
      </Router>
    )
  }
}


