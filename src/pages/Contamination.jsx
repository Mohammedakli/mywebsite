import React, { Component } from 'react';
import Logo from '../img/covid.png'
import WashHand from '../img/wash.png'
import maskImg from '../img/mask.png'


export default class AboutUs extends Component {
   

  render(){
    return(
      <div>
      <div style = {{backgroundColor : 'white', marginTop : -40, backgroundColor : 'black', opacity : 0.80}}>
       <nav>
         
      </nav> 
      < img  style={
        {
         width : 70,
         height : 70,
         marginTop : 50,
         
        }
      } src = {Logo} alt="spinevision logo"></img> 
      <h3 href = {'/'}  style = {{color : 'grey', marginTop : -70, marginLeft : 790, backgroundColor : 'black'}}><a href = {'/'} style = {{color : '#E9E4D9', textDecoration : 'none'}} >Home</a></h3>
      <h3 style = {{color : 'FCD084', marginTop : -46, marginLeft : 900, backgroundColor : 'black'}}><a href = {'/AvoidContamination'} style = {{color : '#E9E4D9', textDecoration : 'none'}} >How to avoid cotamination</a></h3>
      <h3 href = {'/avoidContamination'} style = {{color : 'FCD084', marginTop : -46, marginLeft : 1200, backgroundColor : 'black'}}><a href = {'/AboutUs'} style = {{color : '#E9E4D9', textDecoration : 'none'}} >About Us</a></h3>

      </div>
      <div style = {{width : 1300, height : 650 ,marginLeft : 100,backgroundColor : '#FFF1D6', marginTop : 10, opacity : 0.80, borderRadius : 25}}>
          
      <h1 style = {{textAlign : 'center'}}>How to avoid contamination ?</h1>
      <div>< img  style={
        {
         width : 200,
         height : 200,
         marginTop : 50,
         marginLeft : 1000

         
        }
      } src = {WashHand} alt="spinevision logo"></img> </div>
      <div>< img  style={
        {
         width : 150,
         height : 200,
         marginTop : 10,
         marginLeft : 1050

         
        }
      } src = { maskImg} alt="spinevision logo"></img> </div>
      <div style = {{marginTop : -450}}><p style = {{marginLeft : 50}}><strong>1 -</strong> Keep a distance of at least 1.5 meters from other workers. The ideal distance, especially in places where air conditioning <br></br> can help spread aerosolised particles, is between 3 to 4 meters. If you find it necessary, contact the person in charge the <br></br> day before to remind them that the area should be clear or unoccupied.</p>
          <p style = {{marginLeft : 50}}><strong>2 -</strong> To avoid contamination and being infected, wear nitrile gloves while performing repairs. Never wear gloves larger than <br></br>your own size, as the looseness in the wrist area will compromise your safety. </p>
          <p style = {{marginLeft : 50}}><strong>3 -</strong> When you leave, always disinfect your hands before you drive again. Repeat these precautions throughout all day’s work. </p>
          <p style = {{marginLeft : 50}}><strong>4 -</strong> dismiss all greetings and handshakes </p>
          <p style = {{marginLeft : 50}}><strong>5 -</strong> Avoid touching your eyes, nose and mouth. </p>
          <p style = {{marginLeft : 50}}><strong>7 -</strong> Avoid touching your eyes, nose and mouth. </p>
          <p style = {{marginLeft : 50}}><strong>8 -</strong> If you use public transportation to work, avoid touching the support handles. Whenever possible, try to maintain a safe<br></br> distance of 1.5 meters from other passengers. </p>
          <p style = {{marginLeft : 50, marginTop : 40}}><strong>Together against the Coronavirus. </strong> </p>
          <p style={{ marginTop : 90, textAlign : "center", paddingLeft : -30, fontSize : 15}}>© Copyright 2020 - Covid-1m Team</p></div>
      </div>
      </div>
    )
  }

}