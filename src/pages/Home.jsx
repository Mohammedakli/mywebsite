import React, { Component } from 'react';
import {Line} from 'react-chartjs-2'
import axios from 'axios'
import Logo from '../img/covid.png'
import './Home.css'
import ImgCovid from '../img/tac.jpg' 
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Card } from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';
import CountriesList from './../SpecificComp/countiresList.js';
import 'react-dates/initialize';
import { DateRangePicker } from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import '../Styles/react_dates_overrides.css'; 
import moment from 'moment';
import { default as NumberFormat } from 'react-number-format';


const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);
var filtrerDate = function(tab, start, end){
  return tab.filter(function(item){
    var aDate = new Date(item.Date);
    return aDate >= new Date(start) && aDate <= new Date(end);
});  

}

var filtrerPaysEtDate = function(tab, country, start, end){
  
  // convert strings to Date objects
  var countryTab =  tab.filter(d => d.CountryCode === country && d.Province == "" && d.City == "");  
  return filtrerDate(countryTab, start, end);
}

var getCases = function(tab){
 var cases = []; 
 for(var i = 0;  i < tab.length; i++){
   cases.push(tab[i].Cases); 
 }

 return cases; 
}

var getLabels = function(startDate, stopDate){
  var dateArray = new Array();
  var currentDate = startDate;
  const options = { year: 'numeric', month: 'long', day: 'numeric' };
  while (currentDate <= stopDate) {
      dateArray.push(currentDate.toLocaleDateString(undefined, options));
      currentDate = currentDate.addDays(1);
  }
  return dateArray;
}

Date.prototype.addDays = function(days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
}

export default class Home extends Component {
    constructor(props){
      super(props);
      this.handleChangeCountry = this.handleChangeCountry.bind(this);

     this.state = {
        data: {
          labels : [],
          datasets : [
            {
              label : "Morocco", 
              backgroundColor : "rgba(255, 50, 2, 0.2)",
              data : [] 
            }
          ]
        }, 
        stats:[], 
        recovery:[],
        firstStartDate : new Date('2020-04-01'), 
        firstEndDate: new Date('2020-04-30'), 
        country: 'DE/Germany'
      }
    }
    
    componentDidMount() {
      fetch('https://api.covid19api.com/country/morocco/status/confirmed/live')
      .then(res => res.json())
      .then((data) => {
      this.setState({
        data: {
          labels : getLabels(this.state.firstStartDate, this.state.firstEndDate),
          datasets : [
            {
              label : "Morocco", 
              backgroundColor : "rgba(255, 50, 2, 0.2)",
              data : getCases(filtrerPaysEtDate(data, "MA", this.state.firstStartDate, this.state.firstEndDate)) 
            }
          ]
        }

      }
        )
      })
      .catch(console.log)

      fetch('https://api.covid19api.com/summary')
      .then(res => res.json())
      .then((data) => { 
        var deaths = data.Countries.sort((a, b) => parseFloat(b.TotalDeaths) - parseFloat(a.TotalDeaths))
        deaths = deaths.slice(0, 9); 
        var recovery = data.Countries.sort((a, b) => parseFloat(b.TotalConfirmed - b.TotalRecovered) - parseFloat(a.TotalConfirmed - a.TotalRecovered))
        recovery = recovery.slice(0, 9); 
        this.setState({
          stats : deaths, 
          recovery: recovery
        }
          )
      })
      .catch(console.log)

    }

    onCountryChange = function(country, countryCode, start, end){
      fetch('https://api.covid19api.com/country/' + country + '/status/confirmed/live')
      .then(res => res.json())
      .then((data) => {
      this.setState({
        data: {
          labels : getLabels(start, end),
          datasets : [
            {
              label : country, 
              backgroundColor : "rgba(255, 50, 2, 0.2)",
              data : getCases(filtrerPaysEtDate(data, countryCode, start, end)) 
            }
          ]
        }
      }
      
    )
    
    })
    .catch(console.log)
    }


    handleSelect(date){
      console.log(date); // Momentjs object
  }

    handleChangeCountry(childValue){       
      this.setState({ country: childValue })
      // setTimeout(function(){ console.log(this.state);  }, 3000);
      // console.log(this.state); 
      this.setState({country: childValue}, function() {
        var elt = this.state.country.split('/'); 
        this.onCountryChange(elt[1], elt[0], new Date(moment(this.state.startDate).format('YYYY-MM-DD')), new Date(moment(this.state.endDate).format('YYYY-MM-DD'))); 
      })
    }

    handleChangeDate(start, end){
        this.setState({startDate: start, endDate: end}, function(){          
          if(end > start){
            // call country change
            var elt = this.state.country.split('/'); 
            this.onCountryChange(elt[1], elt[0], new Date(moment(this.state.startDate).format('YYYY-MM-DD')), new Date(moment(this.state.endDate).format('YYYY-MM-DD'))); 
            
          }
        })
   
    }

  render(){
    const options = [{value: '1', label: 'test1'}, {value: '2', label: 'test2'}]
    
    const statItems = []; 
    for(var i = 0; i < this.state.stats.length; i++){
      statItems.push(
        <TableRow key={i}>
        <StyledTableCell component="th" scope="row">
        {this.state.stats[i].Country}
        </StyledTableCell>
        <StyledTableCell align="right">
          <NumberFormat value={this.state.stats[i].TotalConfirmed} thousandSeparator={true} thousandsGroupStyle="thousand" displayType={'text'} />
        </StyledTableCell>
        <StyledTableCell align="right">
          <NumberFormat value={this.state.stats[i].TotalDeaths} displayType={'text'} thousandSeparator={true} thousandsGroupStyle="thousand" />
        </StyledTableCell>
        <StyledTableCell align="right">
          <NumberFormat value={this.state.stats[i].TotalRecovered} displayType={'text'} thousandSeparator={true} thousandsGroupStyle="thousand" />
        </StyledTableCell>
        <StyledTableCell align="right">
          <NumberFormat value={this.state.stats[i].NewConfirmed} displayType={'text'} thousandSeparator={true} thousandsGroupStyle="thousand" />
        </StyledTableCell>
      </TableRow>
      )
 
    }

    const statItemsRecovery = []; 
    for(var i = 0; i < this.state.recovery.length; i++){
      statItemsRecovery.push(
        <TableRow key={i}>
        <TableCell component="th" scope="row">
        {this.state.recovery[i].Country}
        </TableCell>
        <TableCell align="right">
          <NumberFormat value={this.state.recovery[i].TotalRecovered} displayType={'text'} thousandSeparator={true} thousandsGroupStyle="thousand" />
        </TableCell>
      </TableRow>
      )
 
    }



    return(
    
      <div>
        
      <div style = {{backgroundColor : 'white', marginTop : -40, backgroundColor : 'black', opacity : 0.80}}>
     
      < img  style={
        {
         width : 70,
         height : 70,
         marginTop : 50,
         
        }
      } src = {Logo} alt="spinevision logo"></img> 
      <h3 href = {'/'}  style = {{color : 'grey', marginTop : -70, marginLeft : 790, backgroundColor : 'black'}}><a href = {'/'} style = {{color : '#E9E4D9', textDecoration : 'none'}} >Home</a></h3>
      <h3 style = {{color : 'FCD084', marginTop : -46, marginLeft : 900, backgroundColor : 'black'}}><a href = {'/AvoidContamination'} style = {{color : '#E9E4D9', textDecoration : 'none'}} >How to avoid cotamination</a></h3>
      <h3 href = {'/avoidContamination'} style = {{color : 'FCD084', marginTop : -47, marginLeft : 1200, backgroundColor : 'black'}}><a href = {'/AboutUs'} style = {{color : '#E9E4D9', textDecoration : 'none'}} >About Us</a></h3>

      </div>
      
      <div style = {{width : 1300, height : 500 ,marginLeft : 100,backgroundColor : '#FFF1D6', marginTop : 0, opacity : 0.80, borderRadius : 25}}>
      <div><h1 style = {{fontSize : 60, textAlign : "center"}}>COVID19 Statistics</h1></div>
      <div>
      <DateRangePicker 
  startDate={this.state.startDate} // momentPropTypes.momentObj or null,
  startDateId="1" // PropTypes.string.isRequired,
  endDate={this.state.endDate} // momentPropTypes.momentObj or null,
  endDateId="2" // PropTypes.string.isRequired,
  onDatesChange={({ startDate, endDate }) => this.handleChangeDate(startDate, endDate)} // PropTypes.func.isRequired,
  focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
  onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
  isOutsideRange={() => false}
/>
            </div>
      <div>
        <CountriesList onChange={this.handleChangeCountry}/> 
      </div>
      <div>
      
      </div>
      <div style = {{position : "relative", width : 800, height:300, marginLeft : 260,marginTop : -300, background : 'FCD084', borderRadius :  25 }}>
        <Line options = {{
          responsive : true
        }} data = {this.state.data} 
        
        />
        
        </div>
      </div>
      <div style = {{width : 640, height : 500 ,marginLeft : 100,backgroundColor : '#FDF6E8', marginTop : -8, opacity : 0.90, borderRadius : 25}}>
          <h1 style = {{fontSize : 30, textAlign : "center"}}>Regions Risk Ranking</h1>
          {/* <TableContainer component={Paper} style = {{width : 600, marginLeft : 20}}>
      <Table  size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell>Countries</TableCell>
            <TableCell align="right">Confirmed</TableCell>
            <TableCell align="right">Deaths</TableCell>
            <TableCell align="right">Recovered</TableCell>
            <TableCell align="right">New confirmed</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
            {statItems}
          
        </TableBody>
      </Table>
    </TableContainer> */}
   <TableContainer component={Paper}  style = {{width : 600 , height : 400, marginLeft : 20}}>
      <Table  aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Countries</StyledTableCell>
            <StyledTableCell align="right">Confirmed</StyledTableCell>
            <StyledTableCell align="right">Deaths</StyledTableCell>
            <StyledTableCell align="right">Recovered</StyledTableCell>
            <StyledTableCell align="right">New confirmed</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        {statItems}
        </TableBody>
      </Table>
    </TableContainer>
      </div>
      
      <div style = {{width : 640, height : 500 ,marginLeft : 757,backgroundColor : '#FDF6E8', marginTop : -520, opacity : 0.90, borderRadius : 25}}>
        <h1 style = {{fontSize : 30, textAlign : "center"}}>Regions Treatment Efficiency Ranking</h1>
        <TableContainer component={Paper}  style = {{width : 600 , height : 400, marginLeft : 20}}>
      <Table  aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Countries</StyledTableCell>
            <StyledTableCell align="right">Recovered</StyledTableCell>
            
          </TableRow>
        </TableHead>
        <TableBody>
          
            {statItemsRecovery}
          
        </TableBody>
      </Table>
    </TableContainer>
      </div> 
      <div style = {{width :1300, height : 30 ,marginLeft : 100,backgroundColor : '#FDF6E8', marginTop : -40, opacity : 0.80, borderRadius : 25}}>
        <p style={{ marginTop : 55, textAlign : "center", paddingLeft : -30, fontSize : 15}}>© Copyright 2020 - Covid-1m Team</p>
      </div>   
      
     
      
      </div>
    )
  }

}

